/*
 * InternetConnectionRepository.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:17
 * Copyright (c) 2022.
 */

package com.mx.network.internetConnection

import androidx.lifecycle.LiveData

interface InternetConnectionRepository  {
    /* */
    val isOnline: Boolean
    /* */
    val isOnLineLiveData: LiveData<Boolean>

    /** */
    suspend fun fetch()
}