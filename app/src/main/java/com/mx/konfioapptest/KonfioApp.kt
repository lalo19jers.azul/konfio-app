/*
 * KonfioApp.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 13:43
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest

import android.app.Application
import com.mx.konfioapptest.di.initKoin
import timber.log.Timber

class KonfioApp: Application() {

    /** */
    override fun onCreate() {
        super.onCreate()
        initKoin()
        Timber.plant(Timber.DebugTree())
    }

}