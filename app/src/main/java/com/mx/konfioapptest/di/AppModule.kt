/*
 * AppModule.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 13:44
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.di

import com.mx.datasource.httpClientModule
import com.mx.dogs.dogsModule
import com.mx.konfioapptest.KonfioApp
import com.mx.konfioapptest.di.presentation.homeModule
import com.mx.network.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.Module

/**
 * This class is our dependency injection. Built with koin.
 * Our App architecture is Clean Architecture + MVVM.
 * And we change the Viewmodel factory with this dependency injection.
 * If you need more parameters will be welcome in this module.
 */
fun KonfioApp.initKoin() {
    startKoin {
        val modules = getPresentationModules() + getSharedModules() + getFeatureModules()
        androidLogger(Level.ERROR)
        androidContext(applicationContext)
        modules(modules)
    }
}

/**
 *
 * @return [List]
 */
private fun getSharedModules(): List<Module> = listOf(
    /** **/
    networkModule,
    httpClientModule
)

/**
 *
 * @return [List]
 */
private fun getFeatureModules(): List<Module> = listOf(
    dogsModule
)

/**
 * Presentation module is the layer that interacts with UI.
 * Presentation layer contains ViewModel, Fragments and Activities.
 * @return [List]
 */
private fun getPresentationModules(): List<Module> = listOf(
    /**  **/
    homeModule

)