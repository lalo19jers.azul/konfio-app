/*
 * HomeModule.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 13:57
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.di.presentation

import com.mx.konfioapptest.presentation.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel { HomeViewModel(getDogs = get()) }
}