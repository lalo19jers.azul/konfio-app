/*
 * MainActivity.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 13:43
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.presentation

import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.mx.dogs.domain.entity.Dog
import com.mx.dogs.presentation.getDogs.GetDogsStatus
import com.mx.domain.Status
import com.mx.konfioapptest.R
import com.mx.konfioapptest.common.extensions.hideProgressBar
import com.mx.konfioapptest.common.extensions.presentShortSnackBar
import com.mx.konfioapptest.common.extensions.showProgressBar
import com.mx.konfioapptest.databinding.ActivityMainBinding
import com.mx.konfioapptest.presentation.adapter.DogAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    //region INIT COMPONENTS
    /* */
    private val binding: ActivityMainBinding
            by lazy { ActivityMainBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: HomeViewModel by viewModel()

    /* */
    private val dogAdapter: DogAdapter by lazy { DogAdapter() }

    //endregion


    //region LIFECYCLE
    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        getDogs()
    }

    //endregion

    //region LIFECYCLE
    /** */
    private fun getDogs() {
        viewModel.getDogsAsLiveData().observe(
            this, handleStatusObserver()
        )
    }

    /** */
    private fun handleStatusObserver() = Observer<GetDogsStatus> {
        this.hideProgressBar()
        when (it) {
            is Status.Loading -> this.showProgressBar()
            is Status.Error -> binding.root.presentShortSnackBar(it.failure.toString())
            is Status.Done -> setUpView(it.value.dogs)
        }
    }
    //endregion

    //region FUNCTIONS
    private fun setUpView(dogs: List<Dog>) {
        binding.apply {
            dogAdapter.submitList(dogs)
            rvDogs.adapter = dogAdapter
        }
    }

    //endregion
}