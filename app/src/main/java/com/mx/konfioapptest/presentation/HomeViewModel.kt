/*
 * HomeViewModel.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 13:48
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.presentation

import androidx.lifecycle.ViewModel
import com.mx.dogs.presentation.getDogs.GetDogs

class HomeViewModel(
    getDogs: GetDogs
): ViewModel(), GetDogs by getDogs