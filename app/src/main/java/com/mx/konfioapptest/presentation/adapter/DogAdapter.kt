/*
 * DogAdapter.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:59
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mx.dogs.domain.entity.Dog

class DogAdapter: ListAdapter<Dog, DogViewHolder>(MovieDiffUtil()) {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder =
        DogViewHolder.from(parent)

    /** */
    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        val dog = getItem(position)
        holder.bind(dog = dog)
    }

    /** */
    internal class MovieDiffUtil : DiffUtil.ItemCallback<Dog>() {
        /** */
        override fun areItemsTheSame(
            oldItem: Dog,
            newItem: Dog
        ): Boolean = oldItem == newItem

        /** */
        override fun areContentsTheSame(
            oldItem: Dog,
            newItem: Dog
        ): Boolean = oldItem == newItem

    }
}