/*
 * DogViewHolder.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:59
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mx.dogs.domain.entity.Dog
import com.mx.konfioapptest.R
import com.mx.konfioapptest.common.extensions.loadImage
import com.mx.konfioapptest.databinding.ItemDogRowBinding

class DogViewHolder(
    private val binding: ItemDogRowBinding
) : RecyclerView.ViewHolder(binding.root) {

    /** */
    fun bind(dog: Dog) {
        binding.run {
            ivDog.loadImage(dog.image)
            tvDogName.text = dog.dogName
            tvDescription.text = dog.description
            tvAge.text = root.context.getString(R.string.dog_year, dog.age.toString())
        }
    }

    /**
     * Inflates item
     */
    companion object {
        fun from(parent: ViewGroup): DogViewHolder {
            val layoutInflater = ItemDogRowBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return DogViewHolder(layoutInflater)
        }
    }
}