/*
 * View+Extension.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:54
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.common.extensions

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.mx.konfioapptest.R

/** */
fun Fragment.showProgressBar(messageRes: Int = R.string.default_loading_message) =
    requireContext().showProgressBar(messageRes)

/** */
fun Fragment.hideProgressBar() = requireContext().hideProgressBar()

fun View.presentShortSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}