/*
 * String+Extension.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:54
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.common.extensions

import android.util.Patterns

/** */
fun String.isValidUrl() =
    Patterns.WEB_URL.matcher(this).matches()