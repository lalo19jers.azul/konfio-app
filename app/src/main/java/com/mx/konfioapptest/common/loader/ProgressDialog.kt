/*
 * ProgressDialog.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:52
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.common.loader

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.mx.konfioapptest.R
import com.mx.konfioapptest.databinding.ProgressDialogBinding

object ProgressDialog {

    /* */
    @SuppressLint("StaticFieldLeak")
    private lateinit var binding: ProgressDialogBinding
    /* */
    private var dialog: Dialog? = null

    /** */
    fun show(context: Context, message: String? = null) {
        binding = ProgressDialogBinding.inflate(LayoutInflater.from(context))
        dialog = Dialog(context, R.style.AppTheme_FullDialog)
        binding.dialogProgressCircleMessage.visibility = if (message.isNullOrBlank()) View.GONE else View.VISIBLE
        binding.dialogProgressCircleMessage.text = message
        dialog?.setContentView(binding.root)
        dialog?.setCancelable(false)
        dialog?.show()
    }

    /** */
    fun dismiss() {
        if (dialog == null) return
        dialog?.dismiss()
    }
}