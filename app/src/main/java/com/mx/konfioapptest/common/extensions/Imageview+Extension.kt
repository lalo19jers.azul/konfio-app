/*
 * Imageview+Extension.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:53
 * Copyright (c) 2022.
 */

package com.mx.konfioapptest.common.extensions

import android.widget.ImageView
import coil.api.load
import coil.size.Scale
import com.mx.konfioapptest.R

/** */
fun ImageView.loadImage(url: String?) {
    url?.let {
        if (it.isValidUrl())
            this.load(url) {
                error(R.drawable.dog_placeholder)
                scale(Scale.FIT)
            }
        else load(R.drawable.dog_placeholder)
    }

}