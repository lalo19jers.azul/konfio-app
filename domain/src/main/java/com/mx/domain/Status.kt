/*
 * Status.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:05
 * Copyright (c) 2022.
 */

package com.mx.domain

sealed class Status<F: Failure, T> {

    /**
     *
     *
     */
    class Loading<F: Failure, T> : Status<F, T>()

    /**
     *
     *
     */
    data class Error<F: Failure, T>(
        val failure: F
    ) : Status<F, T>()

    /**
     *
     * @param value
     */
    data class Done<F: Failure, T>(
        val value: T
    ) : Status<F, T>()

}