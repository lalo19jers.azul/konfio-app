/*
 * Failure.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:05
 * Copyright (c) 2022.
 */

package com.mx.domain

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureFailure] class.
 */
sealed class Failure {

    /**
     * Extend this class for feature specific failures.
     */
    abstract class FeatureFailure: Failure()

}