/*
 * Exception+Extension.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:42
 * Copyright (c) 2022.
 */

package com.mx.datasource.exception

/** */
fun Exception.message(): String = message ?: tag

/** */
val Exception.tag: String
    get() = "${javaClass.simpleName} - $message"