/*
 * RetrofitApiCall.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:48
 * Copyright (c) 2022.
 */

package com.mx.datasource.remote.model

import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response

/**
 * Handler any retrofit request and returns POJO.
 * @param apiCall Gets retrofit request with suspend keyboard and generates response
 */
@Throws(HttpException::class, Exception::class)
suspend fun <T>retrofitApiCall(apiCall: suspend () -> Response<T>): T {
    val response = apiCall()
    return if (response.isSuccessful)
        response.body()!!
    else throw HttpException(response)
}

fun HttpException.errorMessage(): String = try {
    val errorBody = JSONObject(response()?.errorBody()?.string()!!)
    errorBody.getString("message") ?: message()
} catch (exception: Exception) {
    message()
}