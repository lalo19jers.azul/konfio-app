/*
 * HeaderInterceptorImpl.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:47
 * Copyright (c) 2022.
 */

package com.mx.datasource.remote.httpClient

class HeaderInterceptorImpl: HeaderInterceptor {

    /**
     * @return [String]
     */
    override fun getAuthorizationType(): String = "JWT"

    /**
     * @return [String]
     */
    override fun getAuthorizationValue(): String? {
        return null
    }

}