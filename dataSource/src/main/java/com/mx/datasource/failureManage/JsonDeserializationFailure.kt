/*
 * JsonDeserializationFailure.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:43
 * Copyright (c) 2022.
 */

package com.mx.datasource.failureManage

/** */
interface JsonDeserializationFailure {

    /** */
    val message: String?

}