/*
 * HttpFailure.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:43
 * Copyright (c) 2022.
 */

package com.mx.datasource.failureManage

/** */
interface HttpFailure {

    /** Server failure code */
    val code: Int

    /** Server failure message */
    val message: String

}