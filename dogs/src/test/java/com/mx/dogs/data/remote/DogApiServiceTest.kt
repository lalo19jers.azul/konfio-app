/*
 * DogApiServiceTest.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 14:04
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.remote

import com.mx.dogs.data.dataSource.remote.DogApiService
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.InputStreamReader

class DogApiServiceTest {
    /* */
    private val mockWebServer = MockWebServer()

    /* */
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    /* */
    private val apiService by lazy {
        retrofit.create(DogApiService::class.java)
    }

    /* */
    private lateinit var getDogsJson: String

    /** */
    @Before
    fun setUp() {
        val readerGetDogs=
            InputStreamReader(this.javaClass.classLoader?.getResourceAsStream("get_dogs_response.json"))

        getDogsJson = readerGetDogs.readText()

        readerGetDogs.close()
    }

    @Test
    fun `get dogs response returns success deserialization from example json`() {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(getDogsJson)
                .setResponseCode(200)
        )

        val result = runBlocking { apiService.getDogs() }

        Assert.assertNotNull(result.body())
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}