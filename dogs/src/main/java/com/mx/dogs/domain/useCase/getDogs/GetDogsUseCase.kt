/*
 * GetDogsUseCase.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:57
 * Copyright (c) 2022.
 */

package com.mx.dogs.domain.useCase.getDogs

import com.mx.dogs.domain.DogRepository
import com.mx.domain.Either
import com.mx.domain.UseCase

class GetDogsUseCase(
    private val repository: DogRepository
): UseCase<GetDogsResponse, GetDogsParams, GetDogsFailure>() {

    /** */
    override suspend fun run(
        params: GetDogsParams
    ): Either<GetDogsFailure, GetDogsResponse> =
        repository.getDogs()

}