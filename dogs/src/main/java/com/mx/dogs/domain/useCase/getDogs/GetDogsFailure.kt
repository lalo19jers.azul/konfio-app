/*
 * GetDogsFailure.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:56
 * Copyright (c) 2022.
 */

package com.mx.dogs.domain.useCase.getDogs

import com.mx.datasource.failureManage.HttpFailure
import com.mx.domain.Failure

sealed class GetDogsFailure : Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : GetDogsFailure()

    /* */
    object NoEventsFound : GetDogsFailure()

    /* */
    object NullPointerException: GetDogsFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : GetDogsFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetDogsFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : GetDogsFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is GetDogsFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}