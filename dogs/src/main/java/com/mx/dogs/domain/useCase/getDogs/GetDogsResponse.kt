/*
 * GetDogsResponse.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:56
 * Copyright (c) 2022.
 */

package com.mx.dogs.domain.useCase.getDogs

import com.mx.dogs.domain.entity.Dog

data class GetDogsResponse(
    val dogs: List<Dog>
)