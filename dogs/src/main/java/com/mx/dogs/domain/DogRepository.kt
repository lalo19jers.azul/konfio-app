/*
 * DogRepository.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:58
 * Copyright (c) 2022.
 */

package com.mx.dogs.domain

import com.mx.dogs.domain.useCase.getDogs.GetDogsFailure
import com.mx.dogs.domain.useCase.getDogs.GetDogsResponse
import com.mx.domain.Either

interface DogRepository {

    /** */
    suspend fun getDogs(): Either<GetDogsFailure, GetDogsResponse>
}