/*
 * Dog.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 18:17
 * Copyright (c) 2022.
 */

package com.mx.dogs.domain.entity

data class Dog(
    val dogName: String,
    val description: String,
    val age: Int,
    val image: String
)
