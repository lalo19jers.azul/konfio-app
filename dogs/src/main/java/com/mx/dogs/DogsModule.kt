/*
 * DogsModule.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:51
 * Copyright (c) 2022.
 */

package com.mx.dogs

import com.mx.dogs.data.dataSource.DogRemoteDataSource
import com.mx.dogs.data.dataSource.DogRepositoryImpl
import com.mx.dogs.data.dataSource.remote.DogApiService
import com.mx.dogs.data.dataSource.remote.DogRemoteDataSourceImpl
import com.mx.dogs.domain.DogRepository
import com.mx.dogs.domain.useCase.getDogs.GetDogsUseCase
import com.mx.dogs.presentation.getDogs.GetDogs
import com.mx.dogs.presentation.getDogs.GetDogsImpl
import org.koin.dsl.module
import retrofit2.Retrofit

val dogsModule = module {
    /** PRESENTATION */
    single<GetDogs> { GetDogsImpl(getDogsUseCase = get()) }

    /** USE CASE */
    factory { GetDogsUseCase(repository = get()) }

    /** REPOSITORY */
    single<DogRepository> {
        DogRepositoryImpl(
            dogRemoteDataSource = get(),
            internetConnectionRepository = get()
        )
    }

    /** DATA SOURCE REMOTE */
    single<DogRemoteDataSource> {
        DogRemoteDataSourceImpl(
            apiService = get()
        )
    }

    /** API SOURCE */
    single { get<Retrofit>().create(DogApiService::class.java) }
}