/*
 * GetDogsImpl.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:57
 * Copyright (c) 2022.
 */

package com.mx.dogs.presentation.getDogs

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.mx.dogs.domain.useCase.getDogs.GetDogsParams
import com.mx.dogs.domain.useCase.getDogs.GetDogsUseCase
import com.mx.domain.Status
import com.mx.domain.onFailure
import com.mx.domain.onRight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class GetDogsImpl(
    private val getDogsUseCase: GetDogsUseCase
) : GetDogs {

    /** */
    override fun getDogsAsLiveData(): LiveData<GetDogsStatus> = flow<GetDogsStatus> {
        emit(Status.Loading())
        getDogsUseCase.run(GetDogsParams)
            .onFailure { emit(Status.Error(it)) }
            .onRight {  emit(Status.Done(it))  }
    }.asLiveData(Dispatchers.IO)

}