/*
 * GetDogs.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:57
 * Copyright (c) 2022.
 */

package com.mx.dogs.presentation.getDogs

import androidx.lifecycle.LiveData
import com.mx.dogs.domain.useCase.getDogs.GetDogsFailure
import com.mx.dogs.domain.useCase.getDogs.GetDogsResponse
import com.mx.domain.Status

typealias GetDogsStatus =
    Status<GetDogsFailure, GetDogsResponse>

interface GetDogs {

    /** */
    fun getDogsAsLiveData(): LiveData<GetDogsStatus>
}