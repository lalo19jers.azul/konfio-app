/*
 * DogRemoteDataSourceImpl.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:54
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.dataSource.remote

import com.mx.datasource.remote.model.retrofitApiCall
import com.mx.dogs.data.dataSource.DogRemoteDataSource
import com.mx.dogs.data.dataSource.remote.failure.toDogFailure
import com.mx.dogs.domain.useCase.getDogs.GetDogsFailure
import com.mx.dogs.domain.useCase.getDogs.GetDogsResponse
import com.mx.domain.Either
import retrofit2.HttpException

internal class DogRemoteDataSourceImpl(
    private val apiService: DogApiService
): DogRemoteDataSource {

    /** */
    override suspend fun getDogs(): Either<GetDogsFailure, GetDogsResponse> =
        try {
            retrofitApiCall {
                apiService.getDogs()
            }.let { dogs ->
                Either.Right(GetDogsResponse(dogs.map { it.toDog() }))
            }
        } catch (e: Exception) {
            val failure = when(e) {
                is HttpException -> e.toDogFailure()
                else -> GetDogsFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }
}