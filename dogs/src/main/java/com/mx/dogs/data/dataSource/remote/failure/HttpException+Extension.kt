/*
 * HttpException+Extension.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:55
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.dataSource.remote.failure

import com.mx.datasource.remote.model.HttpErrorCode
import com.mx.dogs.domain.useCase.getDogs.GetDogsFailure
import retrofit2.HttpException

/** */
internal fun HttpException.toDogFailure(): GetDogsFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> GetDogsFailure.NoEventsFound
        else -> GetDogsFailure.ServerFailure(code(),message())
    }