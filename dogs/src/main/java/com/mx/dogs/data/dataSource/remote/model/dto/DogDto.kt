/*
 * DogDto.kt
 * KonfioAppTest
 *
 * Created by lalo on 27/2/22 13:24
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.dataSource.remote.model.dto

import com.mx.dogs.domain.entity.Dog
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DogDto(
    @Json(name = "dogName") val dogName: String,
    @Json(name = "description") val description: String,
    @Json(name = "age") val age: Int,
    @Json(name = "image") val image: String
) {

    /** */
    fun toDog(): Dog =
        Dog(dogName = dogName, description = description, age = age, image = image)
}