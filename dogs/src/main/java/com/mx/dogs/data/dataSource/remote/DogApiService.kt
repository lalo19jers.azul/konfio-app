/*
 * DogApiService.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:54
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.dataSource.remote

import com.mx.dogs.data.dataSource.remote.model.dto.DogDto
import retrofit2.Response
import retrofit2.http.GET

interface DogApiService {
    /** */
    @GET(URL.GET_EVENTS)
    suspend fun getDogs(): Response<List<DogDto>>

    private object URL {
        const val GET_EVENTS = "api/945366962796773376"
    }
}