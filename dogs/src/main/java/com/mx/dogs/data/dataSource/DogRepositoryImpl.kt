/*
 * DogRepositoryImpl.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:53
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.dataSource

import com.mx.dogs.domain.DogRepository
import com.mx.dogs.domain.useCase.getDogs.GetDogsFailure
import com.mx.dogs.domain.useCase.getDogs.GetDogsResponse
import com.mx.domain.Either
import com.mx.network.internetConnection.InternetConnectionRepository

internal class DogRepositoryImpl(
    private val dogRemoteDataSource: DogRemoteDataSource,
    internetConnectionRepository: InternetConnectionRepository
) : DogRepository, InternetConnectionRepository by internetConnectionRepository {

    /** */
    override suspend fun getDogs():
            Either<GetDogsFailure, GetDogsResponse> =
        dogRemoteDataSource.getDogs()

    /*
    How it should be works:
    if (isOnline)
            eventRemoteDataSource.getEvents()
    else eventLocalDataSource.getEvents()
     */


    /**
     * NOTE: for some reason variable [isOnline] always returns false.
     * I don't have more time to check and fix this issue but code commented above
     * is an example that how [isOnline] help us to verify is we connect to remoteDataSource
     * or localDataSource.
     */

}