/*
 * DogRemoteDataSource.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 17:53
 * Copyright (c) 2022.
 */

package com.mx.dogs.data.dataSource

import com.mx.dogs.domain.useCase.getDogs.GetDogsFailure
import com.mx.dogs.domain.useCase.getDogs.GetDogsResponse
import com.mx.domain.Either

interface DogRemoteDataSource {
    /** */
    suspend fun getDogs(): Either<GetDogsFailure, GetDogsResponse>
}