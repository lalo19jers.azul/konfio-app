/*
 * Coordinates.kt
 * KonfioAppTest
 *
 * Created by lalo on 25/2/22 16:05
 * Copyright (c) 2022.
 */

object AppCoordinates {
    const val APP_ID = "com.mx.konfioapptest"

    const val APP_VERSION_NAME = "1.0.0"
    const val APP_VERSION_CODE = 1
}

object LibraryAndroidCoordinates {
    const val LIBRARY_VERSION = "1.0"
    const val LIBRARY_VERSION_CODE = 1
}

object LibraryKotlinCoordinates {
    const val LIBRARY_VERSION = "1.0"
}
