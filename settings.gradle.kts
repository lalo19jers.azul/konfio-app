rootProject.name = ("KonfioAppTest")
include(":app")
include(":dataSource")
include(":domain")
include(":network")
include(":dogs")